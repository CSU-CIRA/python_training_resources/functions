def multi_return():
    return 10, -5.0, "foo"
    
a, b, c = multi_return()
print("a:", a, " b:", b, " c:", c)

as_tuple = multi_return()
print("as_tuple:", as_tuple)
