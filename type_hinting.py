def foo(bar: int, buzz: str) -> None:
    print("bar:", bar, " buzz:", buzz)
    
foo(10, "some string")
