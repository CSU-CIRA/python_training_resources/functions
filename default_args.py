section_separator = "**********"
def default_args(foo, bar=10):
    print("Foo:", foo)
    print("Bar:", bar)
    
default_args(999)
print(section_separator)

default_args(5, 15)
print(section_separator)

default_args(25, bar=-99)
print(section_separator)
