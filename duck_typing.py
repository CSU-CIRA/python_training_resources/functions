def add(a, b):
    return a + b

# This will crash the program since Python 
# doesn't know how to add an integer to a 
# string.    
result = add("foo", 10)
print(result)
