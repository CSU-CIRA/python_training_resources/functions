def add(a, b):
    return a + b

result1 = add(10, 1)
print(result1)

result2 = add(5.0, 3)
print(result2)
